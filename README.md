# Pubmatic problem statement solution

#usage :
mvn clean install

java -jar target/codechallenge-0.0.1-SNAPSHOT-jar-with-dependencies.jar <path of the input stock file>

Example :

java -jar target/codechallenge-0.0.1-SNAPSHOT-jar-with-dependencies.jar E:\stocks\Stocks.txt

#Data will be stored in the csv file name stockData.csv under the project root folder