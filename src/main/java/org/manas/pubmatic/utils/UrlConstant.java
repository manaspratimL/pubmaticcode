package org.manas.pubmatic.utils;
/**
 * 
 * @author Manas
 *
 */
public interface UrlConstant {
	
	String YAHOO_FINANCE_API = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22STOCKCODE%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";
	String STOCK_CODE="STOCKCODE";
}
