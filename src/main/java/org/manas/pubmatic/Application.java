package org.manas.pubmatic;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.manas.pubmatic.services.FileReaderService;
import org.manas.pubmatic.services.YahooAPIService;
/**
 * 
 * @author Manas
 *
 */
public class Application {

	public static void main(String[] args) {
		
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		FileReaderService fileReaderService = new FileReaderService();
		/*Map<String, String> stockCodes = fileReaderService
				.readStockCodes(args[0]);

		Set<String> codes = stockCodes.keySet();*/
		
		List<String> stockCodes = fileReaderService
				.readStockCodes(args[0]);
		
		Iterator<String> itr = stockCodes.iterator();

		while (itr.hasNext()) {
			executorService.execute(new YahooAPIService(itr.next()));
		}
		executorService.shutdown();
	}

}
