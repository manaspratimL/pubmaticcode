package org.manas.pubmatic.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.manas.pubmatic.apiresponse.FinanceAPIResponse;
import org.manas.pubmatic.utils.UrlConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.Gson;

/**
 * 
 * @author Manas
 *
 */
public class YahooAPIService implements Runnable {
	
	private static final Logger logger = LoggerFactory
			.getLogger(YahooAPIService.class);
	
	private HttpClientService httpClient = new HttpClientService();
	private CsvWriterService csvWriterService = CsvWriterService.getInstance();
	private String stockCode;
	
	/**
	 * 
	 */
	public void run() {
		//check in cache or call the API
		fetchAndPersistStockDetails(stockCode);

	}
	/**
	 * 
	 * @param stockCode
	 */
	public YahooAPIService(String stockCode) {
		this.stockCode = stockCode;
	}
	/**
	 * Cache implementation for the Yahoo finanace API
	 */
	private  LoadingCache<String,FinanceAPIResponse> cache = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterAccess(1, TimeUnit.MINUTES)// 1 minute time to live
            .recordStats()
            .build(new CacheLoader<String,FinanceAPIResponse>() {
                @Override
                public FinanceAPIResponse load(String stockCode) throws IOException {
                	FinanceAPIResponse response=finanaceAPI(stockCode);
                	if(response == null){
                		logger.info("Unable to fetch stockCode details from yahoo");
                	}
                    return response;
                }
            });

	
	private void fetchAndPersistStockDetails(String stockCode){
		FinanceAPIResponse apiResponse;
		try {
			apiResponse = cache.get(stockCode);
			csvWriterService.writeToCsvFile(apiResponse.getQuery().getResults()
					.getQuote());
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private FinanceAPIResponse finanaceAPI(String stockCode) {
		FinanceAPIResponse apiResponse=null;
		String stockUrl = UrlConstant.YAHOO_FINANCE_API.replace(
				UrlConstant.STOCK_CODE, stockCode.toUpperCase());
		try {
			HttpResponse response = httpClient.get(stockUrl);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = reader.readLine()) != null) {
				result.append(line);
			}

			Gson gson = new Gson();
			apiResponse = gson.fromJson(result.toString(),
					FinanceAPIResponse.class);
			return apiResponse;
					
		} catch (ClientProtocolException e) {
			logger.error("Yahoo Exception : "+e.getLocalizedMessage());
		} catch (IOException e) {
			logger.error("Yahoo IO Exception : "+e.getLocalizedMessage());
		}
		return apiResponse;
	}
}
