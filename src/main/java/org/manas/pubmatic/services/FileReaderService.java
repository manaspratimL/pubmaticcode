package org.manas.pubmatic.services;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * 
 * @author Manas
 *
 */
public class FileReaderService {

	public List<String> readStockCodes(String filePath) {
		//Map<String, String> stockCodes = new HashMap<String, String>();
		List<String>stockCodes=new ArrayList<String>();
		
		FileReader fr = null;
		BufferedReader br = null;

		try {
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			String code;
			//For map implementation
			/*while ((code = br.readLine()) != null) {
				stockCodes.put(code, code);
			}*/
			while ((code = br.readLine()) != null) {
				stockCodes.add(code);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();

				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return stockCodes;
	}
}
