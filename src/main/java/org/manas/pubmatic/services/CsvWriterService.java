package org.manas.pubmatic.services;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.manas.pubmatic.apiresponse.Quote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Manas
 *
 */
public final class CsvWriterService {
	private static final Logger logger = LoggerFactory
			.getLogger(CsvWriterService.class);
	
	private static CsvWriterService instance = null;

	// Delimiter used in CSV file
	private static final String NEW_LINE_SEPARATOR = "\n";

	// CSV file header
	private static final Object[] FILE_HEADER = { "StockSymbol",
			"Current Price", "Year Target Price", "Year High", "Year Low" };

	// Create the CSVFormat object with "\n" as a record delimiter
	private final CSVFormat csvFileFormat = CSVFormat.DEFAULT
			.withRecordSeparator(NEW_LINE_SEPARATOR);

	private FileWriter fileWriter = null;

	private CSVPrinter csvFilePrinter = null;

	private CsvWriterService() {
		// initialize FileWriter object
		try {
			fileWriter = new FileWriter("stockData.csv", true);
			// initialize CSVPrinter object
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

			// Create CSV file header
			csvFilePrinter.printRecord(FILE_HEADER);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
				csvFilePrinter.close();
			} catch (IOException e) {
				System.out
						.println("Error while flushing/closing fileWriter/csvPrinter !!!");
				e.printStackTrace();
			}
		}
	}

	public synchronized void writeToCsvFile(Quote quote) {

		try {

			fileWriter = new FileWriter("stockData.csv", true);
			// initialize CSVPrinter object
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
			// Write a new quote object to the CSV file
			List<String> stockDataRecord = new ArrayList<String>();
			stockDataRecord.add(quote.getSymbol());
			stockDataRecord.add((quote.getCurrentPrice()!=null?quote.getCurrentPrice():String.valueOf(-1)));
			stockDataRecord.add((quote.getYearTarget()!=null?quote.getYearTarget():String.valueOf(-1)));
			stockDataRecord.add((quote.getYearHigh()!=null?quote.getYearHigh():String.valueOf(-1)));
			stockDataRecord.add((quote.getYearLow()!=null?quote.getYearLow():String.valueOf(-1)));
			csvFilePrinter.printRecord(stockDataRecord);

			logger.info("CSV file was updated successfully for stcokCode : "+quote.getSymbol());
			System.out.println("CSV file was updated successfully for stcokCode : "+quote.getSymbol());
		} catch (Exception e) {
			logger.error("Error in CsvFileWriter !!!"+e.getLocalizedMessage());
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
				csvFilePrinter.close();
			} catch (IOException e) {
				logger.error("Error while flushing/closing fileWriter/csvPrinter !!!");
				e.printStackTrace();
			}
		}
	}

	public synchronized static CsvWriterService getInstance() {
		if (instance == null) {
			instance = new CsvWriterService();
		}
		return instance;
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}
}
