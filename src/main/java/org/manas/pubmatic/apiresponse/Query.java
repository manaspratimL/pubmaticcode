package org.manas.pubmatic.apiresponse;

import java.util.Date;
/**
 * 
 * @author Manas
 *
 */
public class Query {
	
	private int count;
	private Date created;
	private String lang;
	private Result results;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public Result getResults() {
		return results;
	}
	public void setResults(Result results) {
		this.results = results;
	}
}
