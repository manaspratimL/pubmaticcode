package org.manas.pubmatic.apiresponse;

public class FinanceAPIResponse {

	private Query query;

	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

}
