package org.manas.pubmatic.apiresponse;

public class Result {
	private Quote quote;

	public Quote getQuote() {
		return quote;
	}

	public void setQuote(Quote quote) {
		this.quote = quote;
	}
}
