package org.manas.pubmatic.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author Manas
 *
 */
public class Quote {
	private String symbol;
	
	@SerializedName("Change_PercentChange")
	private String changePercentChange;
	
	@SerializedName("EPSEstimateCurrentYear")
	private String currentPrice;
	
	@SerializedName("PriceEPSEstimateCurrentYear")
	private String yearTarget;
	
	@SerializedName("YearLow")
	private String yearLow;
	
	@SerializedName("YearHigh")
	private String yearHigh;
	
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getChangePercentChange() {
		return changePercentChange;
	}
	public void setChangePercentChange(String changePercentChange) {
		this.changePercentChange = changePercentChange;
	}
	public String getYearLow() {
		return yearLow;
	}
	public void setYearLow(String yearLow) {
		this.yearLow = yearLow;
	}
	public String getYearHigh() {
		return yearHigh;
	}
	public void setYearHigh(String yearHigh) {
		this.yearHigh = yearHigh;
	}
	public String getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(String currentPrice) {
		this.currentPrice = currentPrice;
	}
	public String getYearTarget() {
		return yearTarget;
	}
	public void setYearTarget(String yearTarget) {
		this.yearTarget = yearTarget;
	}
	
}
